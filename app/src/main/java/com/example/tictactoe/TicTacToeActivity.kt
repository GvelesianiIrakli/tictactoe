package com.example.tictactoe

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_tic_tac_toe.*

class TicTacToeActivity : AppCompatActivity() {

    private var isPlayerOne: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tic_tac_toe)
        init()
    }

    private fun init() {
        button00.setOnClickListener {
            chnageButton(button00)
        }
        button01.setOnClickListener {
            chnageButton(button01)
        }
        button02.setOnClickListener {
            chnageButton(button02)
        }
        button10.setOnClickListener {
            chnageButton(button10)
        }
        button11.setOnClickListener {
            chnageButton(button11)
        }
        button12.setOnClickListener {
            chnageButton(button12)
        }
        button20.setOnClickListener {
            chnageButton(button20)
        }
        button21.setOnClickListener {
            chnageButton(button21)
        }
        button22.setOnClickListener {
            chnageButton(button22)
        }
        playAgainButton.setOnClickListener {
            buttonContainer.visibility = View.VISIBLE
            resultTextView.text = ""
            isPlayerOne = true
            button00.isClickable = true
            button00.text = ""
            button01.isClickable = true
            button01.text = ""
            button02.isClickable = true
            button02.text = ""
            button10.isClickable = true
            button10.text = ""
            button11.isClickable = true
            button11.text = ""
            button12.isClickable = true
            button12.text = ""
            button20.isClickable = true
            button20.text = ""
            button21.isClickable = true
            button21.text = ""
            button22.isClickable = true
            button22.text = ""
        }
    }


    private fun chnageButton(button: Button) {
        d("click", " button")
        if (isPlayerOne) {
            button.text = "X"
        } else {
            button.text = "O"
        }
        button.isClickable = false
        isPlayerOne = !isPlayerOne
        checkWinner()
    }

    private fun checkWinner() {
        if (button00.text.isNotEmpty() && button00.text.toString() == button01.text.toString() && button00.text.toString() == button02.text.toString()) {
            resultTextView.text = "Winner is ${button00.text}"
            buttonContainer.visibility = View.GONE
        } else if (button10.text.isNotEmpty() && button10.text.toString() == button11.text.toString() && button10.text.toString() == button12.text.toString()) {
            resultTextView.text = "Winner is ${button10.text}"
            buttonContainer.visibility = View.GONE
        } else if (button20.text.isNotEmpty() && button20.text.toString() == button21.text.toString() && button20.text.toString() == button22.text.toString()) {
            resultTextView.text = "Winner is ${button20.text}"
            buttonContainer.visibility = View.GONE
        } else if (button00.text.isNotEmpty() && button01.text.isNotEmpty() && button02.text.isNotEmpty() && button10.text.isNotEmpty() && button20.text.isNotEmpty()
            && button11.text.isNotEmpty() && button12.text.isNotEmpty() && button22.text.isNotEmpty() && button21.text.isNotEmpty()
        ) {
            resultTextView.text = "Game is Draw"
            buttonContainer.visibility = View.GONE
        } else if (button01.text.isNotEmpty() && button01.text.toString() == button11.text.toString() && button01.text.toString() == button21.text.toString()) {
            resultTextView.text = "Winner is ${button01.text}"
            buttonContainer.visibility = View.GONE
        } else if (button02.text.isNotEmpty() && button02.text.toString() == button12.text.toString() && button12.text.toString() == button22.text.toString()) {
            resultTextView.text = "Winner is ${button02.text}"
            buttonContainer.visibility = View.GONE
        } else if (button00.text.isNotEmpty() && button00.text.toString() == button11.text.toString() && button11.text.toString() == button22.text.toString()) {
            resultTextView.text = "Winner is ${button00.text}"
            buttonContainer.visibility = View.GONE
        } else if (button00.text.isNotEmpty() && button00.text.toString() == button10.text.toString() && button10.text.toString() == button20.text.toString()) {
            resultTextView.text = "Winner is ${button00.text}"
            buttonContainer.visibility = View.GONE
        } else if (button20.text.isNotEmpty() && button20.text.toString() == button11.text.toString() && button20.text.toString() == button02.text.toString()) {
            resultTextView.text = "Winner is ${button20.text}"
            buttonContainer.visibility = View.GONE
        }
    }
}